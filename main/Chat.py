#importações dos módulos
from chatterbot import *
from chatterbot.trainers import *
from PyQt5.QtWidgets import *
from sys import argv, exit

class Chat: # classe chat
    chat = ChatBot("Exemplo") # atributo = instância e criação bot

    def carregar(self):
        self.chat.set_trainer(ChatterBotCorpusTrainer) # tipo de treino
        self.chat.train("chatterbot.corpus.portuguese") #treina

    def mensagem(self):
        self.msg = self.en.text() # recebe o texto do line edit
        self.msgAnt = self.te.toPlainText() # recebe o texto do text edit
        self.resp = self.chat.get_response(self.msg) # resposta
        self.te.setText("{} \n Você: {} \n Computador: {}".format(self.msgAnt, self.msg, self.resp)) #inserindo no text edit



    def __init__(self):
        super().__init__()
        self.carregar() # chamando o método  carregar

        self.app = QApplication(argv) # instancia da aplicação

        self.horizontal = QHBoxLayout() # leiaute horizontal
        self.vertical = QVBoxLayout()  # leiaute vertical

        self.lb = QLabel("Mensagem") # texto
        self.en = QLineEdit() # entrada de uma unica linha
        self.te = QTextEdit() # entrada de multiplas linhas
        #self.te.setEnabled(False)
        self.btn = QPushButton("Enviar") # botão
        self.btn.clicked.connect(self.mensagem) # chama o método  mensagem

        self.horizontal.addWidget(self.lb) # adiciona label
        self.horizontal.addWidget(self.en) # adiciona line edit
        self.horizontal.addWidget(self.btn) # adiciona o botao

        self.vertical.addWidget(self.te) # adiciona o text edit
        self.vertical.addLayout(self.horizontal) #adiciona o leiaute horizontal

        self.janela = QWidget() # janela
        self.janela.setWindowTitle("Chat Bot") # Titulo
        self.janela.resize(800,600) # tamanho
        self.janela.setLayout(self.vertical) #Adiciono o leiaute vertical
        self.janela.show() # mostrar a janela




        exit(self.app.exec_()) # fechar aplicação

if __name__ == '__main__': # caso eu chame esse arquivo
    Chat() # instância da classe chat, criando objeto